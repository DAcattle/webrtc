import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:webrtc_example/utils/socker_service.dart';

class PeerWithSignaling extends StatefulWidget {
  final String userId;

  PeerWithSignaling({required this.userId});

  @override
  _PeerWithSignalingState createState() => _PeerWithSignalingState();
}

class _PeerWithSignalingState extends State<PeerWithSignaling> {
  final _socket = SocketService.instance;
  final _localRenderer = RTCVideoRenderer();
  final _remoteRenderer = RTCVideoRenderer();
  MediaStream? _localStream;
  MediaStream? _remoteStream;
  RTCPeerConnection? _peerConnection;
  bool _isConnected = false;

  final Map<String, dynamic> mediaConstraints = {
    'audio': true,
    'video': {
      'mandatory': {
        'minWidth': 640,
        'minHeight': 480,
        'minFrameRate': 30,
      },
      'facingMode': 'user',
      'optional': [],
    }
  };

  final Map<String, dynamic> configuration = {
    'iceServer': [
      {'url': 'stun:stun.l.google.com:19302'}
    ]
  };

  final Map<String, dynamic> pcConstraints = {
    'mandatory': {},
    'optional': [
      {'DtlsSrtpKeyAgreement': true}
    ],
  };

  final Map<String, dynamic> sdpConstraints = {
    'mandatory': {
      'OfferToReceiveAudio': false,
      'OfferToReceiveVideo': true,
    },
    'optional': [],
  };

  @override
  void initState() {
    _initRenderers();
    super.initState();
  }

  _initRenderers() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  _onIceCandidate(RTCIceCandidate candidate) {
    print('on ice candidate');
    _socket.emit('candidate', {'to': widget.userId == '1' ? '2' : '1', 'content': candidate.toMap()});
  }

  _onAddStream(MediaStream stream) {
    print('on add stream');
    _remoteStream = stream;
    _remoteRenderer.srcObject = stream;
  }

  _open() async {
    _localStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
    _localRenderer.srcObject = _localStream;

    _peerConnection = await createPeerConnection(configuration, pcConstraints);
    _peerConnection!.onIceCandidate = _onIceCandidate;
    _peerConnection!.addStream(_localStream!);
    _peerConnection!.onAddStream = _onAddStream;

    final offer = await _peerConnection!.createOffer(sdpConstraints);
    _peerConnection!.setLocalDescription(offer);

    print('offer: $offer');
    _socket.emit('offer', {'to': widget.userId == '1' ? '2' : '1', 'content': offer.toMap()});

    setState(() {
      _isConnected = true;
    });
  }

  _close() async {
    try {
      await _localStream?.dispose();
      await _remoteStream?.dispose();
      await _peerConnection?.close();
      _peerConnection = null;
      _localRenderer.srcObject = null;
      _remoteRenderer.srcObject = null;
    } catch (e) {
      print(e.toString());
    }

    setState(() {
      _isConnected = false;
    });
  }

  void _connect() {
    _socket.connect(widget.userId);

    _socket.on('test', (msg) => print('Msg: $msg'));

    _socket.on('onCandidate', (msg) {
      print('onCandidate: $msg');
      if (msg['to'] == widget.userId) {
        final c = msg['content'];
        final candidate = RTCIceCandidate(c['candidate'], c['sdpMid'], c['sdpMLineIndex']);
        _peerConnection!.addCandidate(candidate);
      }
    });

    _socket.on('onOffer', (msg) async {
      print('onOffer: $msg');
      if (msg['to'] == widget.userId) {
        _localStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
        _localRenderer.srcObject = _localStream.;

        _peerConnection = await createPeerConnection(configuration, pcConstraints);
        _peerConnection!.onIceCandidate = _onIceCandidate;
        _peerConnection!.addStream(_localStream!);
        _peerConnection!.onAddStream = _onAddStream;

        setState(() {
          _isConnected = true;
        });

        final c = msg['content'];
        _peerConnection!.setRemoteDescription(RTCSessionDescription(c['sdp'], c['type']));

        final answer = await _peerConnection!.createAnswer(sdpConstraints);
        _peerConnection!.setLocalDescription(answer);
        _socket.emit('answer', {'to': widget.userId == '1' ? '2' : '1', 'content': answer.toMap()});
      }
    });

    _socket.on('onAnswer', (msg) {
      print('onAnswer: $msg');
      if (msg['to'] == widget.userId) {
        final c = msg['content'];
        _peerConnection!.setRemoteDescription(RTCSessionDescription(c['sdp'], c['type']));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Peer Connection'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return Center(
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: Stack(
                children: [
                  Align(
                    alignment: orientation == Orientation.portrait ? const FractionalOffset(.5, .1) : const FractionalOffset(.0, .5),
                    child: Container(
                      // color: Colors.grey,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      width: 320,
                      height: 240,
                      decoration: BoxDecoration(color: Colors.black54),
                      child: RTCVideoView(_localRenderer),
                    ),
                  ),
                  Align(
                    alignment: orientation == Orientation.portrait ? const FractionalOffset(.5, .9) : const FractionalOffset(1.0, .5),
                    child: Container(
                      // color: Colors.grey,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      width: 320,
                      height: 240,
                      decoration: BoxDecoration(color: Colors.black54),
                      child: RTCVideoView(_remoteRenderer),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: [
            ElevatedButton(
                onPressed: () async {
                  _connect();
                },
                child: Text('Start Socket')),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isConnected ? _close : _open,
        child: Icon(_isConnected ? Icons.close : Icons.add),
      ),
    );
  }
}
