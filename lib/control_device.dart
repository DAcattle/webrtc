import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'dart:core';

class ControlDevice extends StatefulWidget {
  @override
  _ControlDeviceState createState() => _ControlDeviceState();
}

class _ControlDeviceState extends State<ControlDevice> {
  final _localRenderer = RTCVideoRenderer();
  MediaStream? _localStream;
  bool _isOpen = false;
  bool _cameraOff = false;
  bool _microphoneOff = false;
  bool _speakerOn = true;

  @override
  void initState() {
    _initRenderer();
    super.initState();
  }

  _initRenderer() async {
    await _localRenderer.initialize();
  }

  _open() async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': true,
      'video': {'width': 1280, 'height': 720}
    };

    try {
      navigator.mediaDevices.getUserMedia(mediaConstraints).then((stream) {
        _localStream = stream;
        _localRenderer.srcObject = _localStream;
      });
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) return;

    setState(() {
      _isOpen = true;
    });
  }

  _close() async {
    try {
      await _localStream!.dispose();
      _localRenderer.srcObject = null;
    } catch (e) {
      print(e.toString());
    }

    setState(() {
      _isOpen = false;
    });
  }

  _turnCamera() {
    if (_localStream != null && _localStream!.getVideoTracks().length > 0) {
      var muted = !_cameraOff;
      setState(() {
        _cameraOff = muted;
      });
      _localStream!.getVideoTracks()[0].enabled = !muted;
    } else {
      print('Camera turn fail');
    }
  }

  _switchCamera() {
    if (_localStream != null && _localStream!.getVideoTracks().length > 0) {
      Helper.switchCamera(_localStream!.getVideoTracks()[0]);
    } else {
      print('Camera switch fail');
    }
  }

  _turnMicrophone() {
    if (_localStream != null && _localStream!.getVideoTracks().length > 0) {
      var muted = !_microphoneOff;
      setState(() {
        _microphoneOff = muted;
      });
      _localStream!.getAudioTracks()[0].enabled = !muted;
    } else {
      print('Microphone turn fail');
    }
  }

  _switchSpeaker() {
    setState(() {
      _speakerOn = !_speakerOn;
      final audioTrack = _localStream!.getAudioTracks()[0];
      audioTrack.enableSpeakerphone(_speakerOn);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Control Device'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return Center(
            child: Container(
              // color: Colors.grey,
              margin: EdgeInsets.fromLTRB(.0, .0, .0, .0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: RTCVideoView(_localRenderer),
            ),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                icon: Icon(_cameraOff ? Icons.videocam_off : Icons.videocam),
                onPressed: () {
                  _turnCamera();
                }),
            IconButton(
                icon: Icon(Icons.switch_camera),
                onPressed: () {
                  _switchCamera();
                }),
            IconButton(icon: Icon(_microphoneOff ? Icons.mic_off : Icons.mic), onPressed: () {
              _turnMicrophone();
            }),
            IconButton(icon: Icon(_speakerOn ? Icons.volume_up : Icons.volume_down), onPressed: () {
              _switchSpeaker();
            })
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isOpen ? _close : _open,
        child: Icon(_isOpen ? Icons.close : Icons.add),
      ),
    );
  }
}
