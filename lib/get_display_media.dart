import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'dart:core';

class GetDisplayMedia extends StatefulWidget {
  @override
  _GetDisplayMedia createState() => _GetDisplayMedia();
}

class _GetDisplayMedia extends State<GetDisplayMedia> {
  final _localRenderer = RTCVideoRenderer();
  late MediaStream _localStream;
  bool _isOpen = false;

  @override
  void initState() {
    _initRenderer();
    super.initState();
  }

  _initRenderer() async {
    await _localRenderer.initialize();
  }

  _open() async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': true,
      'video': true
    };

    try {
      navigator.mediaDevices.getDisplayMedia(mediaConstraints).then((stream) {
        _localStream = stream;
        _localRenderer.srcObject = _localStream;
      });
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) return;

    setState(() {
      _isOpen = true;
    });
  }

  _close() async {
    try {
      await _localStream.dispose();
      _localRenderer.srcObject = null;
    } catch (e) {
      print(e.toString());
    }

    setState(() {
      _isOpen = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Get Display Media'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return Center(
            child: Container(
              // color: Colors.grey,
              margin: EdgeInsets.fromLTRB(.0, .0, .0, .0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: RTCVideoView(_localRenderer),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isOpen ? _close : _open,
        child: Icon(_isOpen ? Icons.close : Icons.add),
      ),
    );
  }
}
