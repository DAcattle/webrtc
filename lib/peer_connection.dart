import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'dart:core';

class PeerConnection extends StatefulWidget {
  @override
  _PeerConnectionState createState() => _PeerConnectionState();
}

class _PeerConnectionState extends State<PeerConnection> {
  final _localRenderer = RTCVideoRenderer();
  final _remoteRenderer = RTCVideoRenderer();
  RTCPeerConnection? _localConnection;
  RTCPeerConnection? _remoteConnection;
  MediaStream? _localStream;
  MediaStream? _remoteStream;
  bool _isConnected = false;

  final Map<String, dynamic> mediaConstraints = {
    'audio': true,
    'video': {
      'mandatory': {
        'minWidth': 640,
        'minHeight': 480,
        'minFrameRate': 30,
      },
      'facingMode': 'user',
      'optional': [],
    }
  };

  final Map<String, dynamic> sdpConstraints = {
    'mandatory': {
      'OfferToReceiveAudio': false,
      'OfferToReceiveVideo': true,
    },
    'optional': [],
  };

  final Map<String, dynamic> configuration = {
    'iceServer': [
      {'url': 'stun:stun.l.google.com:19302'}
    ]
  };

  final Map<String, dynamic> pcConstraints = {
    'mandatory': {},
    'optional': [
      {'DtlsSrtpKeyAgreement': true}
    ],
  };

  @override
  void initState() {
    _initRenderers();
    super.initState();
  }

  _initRenderers() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  _onLocalCandidate(RTCIceCandidate candidate) {
    print('local candidate: ${candidate.candidate}');
    _remoteConnection?.addCandidate(candidate);
  }


  _onRemoteCandidate(RTCIceCandidate candidate) {
    print('remote candidate: ${candidate.candidate}');
    _localConnection?.addCandidate(candidate);
  }

  _onRemoteAddStream(MediaStream stream) {
    _remoteStream = stream;
    _remoteRenderer.srcObject = stream;
  }

  _open() async {
    if (_localConnection != null || _remoteConnection != null) return;

    try {
      _localStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
      _localRenderer.srcObject = _localStream;

      _localConnection = await createPeerConnection(configuration, pcConstraints);
      _localConnection!.onIceCandidate = _onLocalCandidate;

      _localConnection!.addStream(_localStream!);
      _localStream!.getAudioTracks()[0].setMicrophoneMute(false);

      _remoteConnection = await createPeerConnection(configuration, pcConstraints);
      _remoteConnection!.onIceCandidate = _onRemoteCandidate;
      _remoteConnection!.onAddStream = _onRemoteAddStream;

      final offer = await _localConnection!.createOffer(sdpConstraints);
      print('offer: ${offer.sdp}');
      _localConnection!.setLocalDescription(offer);
      _remoteConnection!.setRemoteDescription(offer);

      final answer = await _remoteConnection!.createAnswer(sdpConstraints);
      print('answer: ${answer.sdp}');
      _remoteConnection!.setLocalDescription(answer);
      _localConnection!.setRemoteDescription(answer);
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) return;

    setState(() {
      _isConnected = true;
    });
  }

  _close() async {
    try {
      await _localStream?.dispose();
      await _remoteStream?.dispose();
      await _localConnection?.close();
      await _remoteConnection?.close();
      _localConnection = null;
      _remoteConnection = null;
      _localRenderer.srcObject = null;
      _remoteRenderer.srcObject = null;
    } catch (e) {
      print(e.toString());
    }

    setState(() {
      _isConnected = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Peer Connection'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return Center(
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: Stack(
                children: [
                  Align(
                    alignment: orientation == Orientation.portrait ? const FractionalOffset(.5, .1) : const FractionalOffset(.0, .5),
                    child: Container(
                      // color: Colors.grey,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      width: 320,
                      height: 240,
                      decoration: BoxDecoration(color: Colors.black54),
                      child: RTCVideoView(_localRenderer),
                    ),
                  ),
                  Align(
                    alignment: orientation == Orientation.portrait ? const FractionalOffset(.5, .9) : const FractionalOffset(1.0, .5),
                    child: Container(
                      // color: Colors.grey,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      width: 320,
                      height: 240,
                      decoration: BoxDecoration(color: Colors.black54),
                      child: RTCVideoView(_remoteRenderer),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isConnected ? _close : _open,
        child: Icon(_isConnected ? Icons.close : Icons.add),
      ),
    );
  }
}
