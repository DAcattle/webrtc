import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webrtc_example/control_device.dart';
import 'package:webrtc_example/get_user_media.dart';
import 'package:webrtc_example/peer_connection.dart';
import 'package:webrtc_example/peer_with_signaling.dart';

import 'get_display_media.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true; // add your localhost detection logic here if you want
  }
}

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WebRTC Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller  = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('WebRTC Example'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
              title: Text('Get User Media'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => GetUserMedia()));
              }),
          ListTile(
              title: Text('Get Display Media'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => GetDisplayMedia()));
              }),
          ListTile(
              title: Text('Control Device'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ControlDevice()));
              }),
          ListTile(
              title: Text('Peer Connection'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => PeerConnection()));
              }),
          ListTile(
              title: Text('Peer With Signaling'),
              subtitle: TextField(
                controller: controller,
                decoration: InputDecoration(
                  hintText: 'Enter your id'
                ),
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => PeerWithSignaling(userId: controller.text)));
              }),
        ],
      ),
    );
  }
}
