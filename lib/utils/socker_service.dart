import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart';

typedef void SocketListenCallBack(dynamic msg);

class SocketService {
  static final _service = SocketService._();

  static SocketService get instance => _service;
  final Socket _socket;
  String? _userId;

  factory SocketService() {
    return _service;
  }

  SocketService._() : _socket = io('http://59.127.187.54:51002', OptionBuilder().setTransports(['websocket']).disableAutoConnect().build()) {
    _socket.onConnect((_) {
      print('connected');
      _socket.emit('test', 'Hello');
    });
    _socket.onConnecting((data) => print('connecting'));
    _socket.onDisconnect((data) => print('disconnect'));
    _socket.onConnectError((data) => print('connect error: $data'));
    _socket.onConnectTimeout((data) => print('timeout'));
    _socket.onReconnect((data) => print('reconnect'));
  }

  void connect(String userId) {
    _userId = userId;
    _socket.connect();
  }

  void disconnect() {
    _socket.dispose();
  }

  void emit(String topic, dynamic payload) {
    this._socket.emit(topic, payload);
  }

  void on(String topic, SocketListenCallBack callBack) {
    _socket.on(topic, callBack);
  }
}
